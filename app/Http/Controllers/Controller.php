<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Collective\Html\HtmlServiceProvider\Form;
use App\Shop;
use App\Robot;
use Request;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function control_panel() {

        $data = array ('alert' => ' ');

        return view('control') -> with ($data);

    }

    public function creating_Shop() {

        $data = array ('alert' => ' ');

        return view('createShop') -> with ($data);

    }

    public function createShop() {

        $Shop = new Shop;
        $input = Request::all();

        if ($input['x'] == ''  || $input['y'] == '') {

        	$data = array ('alert' => 'Please fill in all fields!');

        	return view('createShop') -> with ($data);

        }

        $Shop -> x = $input['x'];
        $Shop -> y = $input['y'];

        $Shop -> save();

        //$results = Shop::all();

        $data = array ('alert' => 'New shop created! your new shop ID is ' . $Shop['id']);

        return view('createShop') -> with ($data);

    }

    public function editing_Shop() {

    	$input = Request::all();

    	$results = DB::table('shopunits') -> where('id', $input) -> first();
    	$resulteir = DB::table('robotunits') -> where('owner', $input) -> get() -> toArray();

        $FilteredResults = (array) $results;
        //$RobotsRetreival = $resulteir -> toArray();

        if ($FilteredResults != null) {

            if ($resulteir != null) {

        	   $data = array ('id' => $results->id, 'x' => $results->x, 'y' => $results->y, 'owner' => $resulteir, 'message' => ' ');
                return view('editShop') -> with ($data);

            }

            else {

                $data = array ('id' => $results->id, 'x' => $results->x, 'y' => $results->y, 'owner' => null, 'message' => ' ');

                //dd($data['owner']);
                return view('editShop') -> with ($data);

            }

        }

        else {

            $data = array ('alert' => 'No matches with input ID');

            return view('control') -> with ($data);

        }

    }

        public function deleteShop() {

            $input = Request::all();

            if ($input['id'] != null) {

                $Shop = DB::table('shopunits')->where('id', $input['id'])->first();

                $Filtered = (array) $Shop;

                if ($Filtered == null) {

                    $data = array ('alert' => 'No matches with input ID');

                    return view('control') -> with ($data);

                }

                else {

                    DB::table('shopunits')->where('id', $input['id'])->delete();

                    $data = array ('alert' => 'Shop removed successfully');

                    return view('control') -> with ($data);

                }

            }

            else {

                $data = array ('alert' => 'Invalid input parameter!');

                return view('control') -> with ($data);

            }

    }

    public function createRobot() {

        $input = Request::all();

        $Shops = DB::table('shopunits') -> where('id', $input['id']) -> first();

        $hits = 0;

        if ($input['x'] > $Shops->x || $input['y'] > $Shops->y) {

            $hits++;

        }

        if ($input['heading'] != 'N' && $input['heading'] != 'E' && $input['heading'] != 'W' && $input['heading'] != 'S') {

            $hits++;

        }

        $keysplitter = str_split($input['commands']);

        foreach ($keysplitter as $key) {

            if ($key != 'L' && $key != 'R' && $key != 'M') {

                $hits++; 

            }

        }

        $resulteir = DB::table('robotunits') -> where('owner', $input['id']) -> get();

        $FileteredRobots = $resulteir -> toArray();

        foreach($FileteredRobots as $Robot) {

            if ($input['x'] == $Robot -> x && $input['y'] == $Robot -> y) {

                $hits++;

            }

        }

        if ($hits == 0) {

            $Robot = new Robot;
            $Robot -> owner = $input['id'];
            $Robot -> x = $input['x'];
            $Robot -> y = $input['y'];
            $Robot -> heading = $input['heading'];
            $Robot -> commands = $input['commands'];

            $Robot -> save();

            $results = DB::table('shopunits') -> where('id', $input['id']) -> first();
            $resulteir = DB::table('robotunits') -> where('owner', $input['id']) -> get() -> toArray();

            if ($resulteir != null) {

                $data = array ('id' => $results->id, 'x' => $results->x, 'y' => $results->y, 'owner' => $resulteir, 'message' => 'New robot created!');
                return view('editShop') -> with ($data);

            }

            else {

                $data = array ('id' => $results->id, 'x' => $results->x, 'y' => $results->y, 'owner' => null, 'message' => 'New robot created!');
                return view('editShop') -> with ($data);

            }

        }

        else {

            $results = DB::table('shopunits') -> where('id', $input['id']) -> first();
            $resulteir = DB::table('robotunits') -> where('owner', $input['id']) -> get() -> toArray();

            if ($resulteir != null) {

                $data = array ('id' => $results->id, 'x' => $results->x, 'y' => $results->y, 'owner' => $resulteir, 'message' => 'Invalid parameters detected, please ensure that the inputs are correct.');
                return view('editShop') -> with ($data);

            }

            else {

                $data = array ('id' => $results->id, 'x' => $results->x, 'y' => $results->y, 'owner' => null, 'message' => 'Invalid parameters detected, please ensure that the inputs are correct.');
                return view('editShop') -> with ($data);

            }
        }

    }



    public function deleteRobot() {

            $input = Request::all();

            DB::table('robotunits')->where('robot_id', $input['robotid'])->delete();

            $results = DB::table('shopunits') -> where('id', $input['id']) -> first();
            $resulteir = DB::table('robotunits') -> where('owner', $input['id']) -> get() -> toArray();

            if ($resulteir != null) {

                $data = array ('id' => $results->id, 'x' => $results->x, 'y' => $results->y, 'owner' => $resulteir, 'message' => 'Robot has been successfully deleted!');
                return view('editShop') -> with ($data);

            }

            else {

                $data = array ('id' => $results->id, 'x' => $results->x, 'y' => $results->y, 'owner' => null, 'message' => 'Robot has been successfully deleted!');
                return view('editShop') -> with ($data);

            }

    }

    public function editRobot() {

            $input = Request::all();

            //dd($input);

            $Shops = DB::table('shopunits') -> where('id', $input['id']) -> first();

            $hits = 0;

            if ($input['x'] > $Shops->x || $input['y'] > $Shops->y) {

                $hits++;

            }

            if ($input['heading'] != 'N' && $input['heading'] != 'E' && $input['heading'] != 'W' && $input['heading'] != 'S') {

                $hits++;

            }

            $keysplitter = str_split($input['commands']);

            foreach ($keysplitter as $key) {

                if ($key != 'L' && $key != 'R' && $key != 'M') {

                    $hits++; 

                }

            }

            $resulteir = DB::table('robotunits') -> where('owner', $input['id']) -> get();

            $FileteredRobots = $resulteir->toArray();

            foreach($FileteredRobots as $Robot) {

                if ($input['x'] == $Robot -> x && $input['y'] == $Robot -> y) {

                    if ($input['robot_id'] == $Robot -> robot_id) {

                    }

                    else {

                        $hits++;

                    }

                }

            }

            if ($hits == 0) {

                DB::table('robotunits')->where('robot_id', $input['robot_id'])->update(array('x' => $input['x'], 'y' => $input['y'], 'heading' => $input['heading'], 'commands' => $input['commands']));

                $results = DB::table('shopunits') -> where('id', $input['id']) -> first();
                $resulteir = DB::table('robotunits') -> where('owner', $input['id']) -> get();

                $data = array ('id' => $results->id, 'x' => $results->x, 'y' => $results->y, 'owner' => $resulteir, 'message' => 'Robot has been successfully edited!');
                return view('editShop') -> with ($data);

            }

            else {

                $results = DB::table('shopunits') -> where('id', $input['id']) -> first();
                $resulteir = DB::table('robotunits') -> where('owner', $input['id']) -> get();

                $data = array ('id' => $results->id, 'x' => $results->x, 'y' => $results->y, 'owner' => $resulteir, 'message' => 'Invalid parameters detected, please ensure that the inputs are correct.');
                return view('editShop') -> with ($data);

            }

    }

    public function simulate_Global() {

        $input = Request::all();

        $results = DB::table('shopunits') -> where('id', $input) -> first();
        $resulteir = DB::table('robotunits') -> where('owner', $input) -> get();

        $FilteredResults = (array) $results;

        $message = [];

        if ($FilteredResults != null) {

            $data = array ('id' => $results->id, 'x' => $results->x, 'y' => $results->y, 'owner' => $resulteir, 'message' => $message);
                
            return view('simulateGlobal') -> with ($data);

        }

        else {

            $data = array ('alert' => 'No matches with input ID');

            return view('control') -> with ($data);

        }

    }

    public function execute_GlobalSimulation() {

        $input = Request::all();

        $Shop = DB::table('shopunits') -> where('id', $input['id']) -> first();

        $RobotListActual = DB::table('robotunits') -> where('owner', $input['id']) -> get() -> toArray();

        $RobotListNext = DB::table('robotunits') -> where('owner', $input['id']) -> get() -> toArray();

        //array_push($Collisions, array(0, 1, 2));

        //dd($Collisions);

        $MoveList = [];

        $CollisionList1 = [];
        $CollisionList2 = [];

        $message = [];

        $i = 0;

        if (count($RobotListActual) < 1) {

        //$data = array ('id' => $Shop->id, 'x' => $Shop->x, 'y' => $Shop->y, 'owner' => $RobotListActual, 'message' => ' ');

        //return view('simulateGlobal') -> with ($data);

        $data = array ('alert' => 'This shop has no Robots');

        return view('control') -> with ($data);

        }   

        foreach ($RobotListActual as $Robot) {

            $MoveList[$i] = count(str_split($Robot -> commands));

            $i++;

        }
 
        $pivot = $MoveList[0];
        $left = array();
        $right = array();
 
        for ($i = count($MoveList) - 2; $i > 0; $i--) {
            if ($MoveList[$i] > $pivot)
                $left[] = $MoveList[$i];
            else
                $right[] = $MoveList[$i];
        }
 
        $SortedMoveList = array_merge($left, array($pivot), $right);

        $Collisions[] = array();

        array_shift($Collisions);

        for ($a = 0; $a < $SortedMoveList[0]; $a++) {

            array_push($Collisions, array());

        }

        //array_push($Collisions, array(1, 2, 3));

        //dd($Collisions);

        //dd($SortedMoveList[0]);

        for ($i = 0; $i < $SortedMoveList[0]; $i++) {

            for ($j = 0; $j < count($RobotListActual); $j++) {


                if ($i >= count(str_split($RobotListActual[$j] -> commands))) {


                }

                else {

                    if (str_split($RobotListActual[$j] -> commands)[$i] == 'M') {

                        if (($RobotListActual[$j] -> heading) == 'N') {

                            $RobotListNext[$j] -> y = $RobotListActual[$j] -> y - 1;

                            //echo('North!!!');

                        }

                        else if (($RobotListActual[$j] -> heading) == 'E') {

                            $RobotListNext[$j] -> x = $RobotListActual[$j] -> x + 1;

                            //echo('East!!!');

                        }

                        else if (($RobotListActual[$j] -> heading) == 'W') {

                            $RobotListNext[$j] -> x = $RobotListActual[$j] -> x - 1;

                            //echo('West!!!');
                            
                        }

                        else if (($RobotListActual[$j] -> heading) == 'S') {

                            $RobotListNext[$j] -> y = $RobotListActual[$j] -> y + 1;

                            //echo('South!!!');
                            
                        }

                        else {

                        }

                    }

                    else if (str_split($RobotListActual[$j] -> commands)[$i] == 'L') {

                        //$RobotListActual[$j] -> heading = 'N';

                        if(($RobotListActual[$j] -> heading) == 'N') {

                            $RobotListActual[$j] -> heading = 'W';

                        }

                        else if(($RobotListActual[$j] -> heading) == 'E') {

                            $RobotListActual[$j] -> heading = 'N';
                            
                        }

                        else if(($RobotListActual[$j] -> heading) == 'W') {

                            $RobotListActual[$j] -> heading = 'S';
                            
                        }

                        else if(($RobotListActual[$j] -> heading) == 'S') {

                            $RobotListActual[$j] -> heading = 'E';
                            
                        }

                    }

                    else if (str_split($RobotListActual[$j] -> commands)[$i] == 'R') {

                        //$RobotListActual[$j] -> heading = 'E';

                        if(($RobotListActual[$j] -> heading) == 'N') {

                            $RobotListActual[$j] -> heading = 'E';

                        }

                        else if(($RobotListActual[$j] -> heading) == 'E') {

                            $RobotListActual[$j] -> heading = 'S';
                            
                        }

                        else if(($RobotListActual[$j] -> heading) == 'W') {

                            $RobotListActual[$j] -> heading = 'N';
                            
                        }

                        else if(($RobotListActual[$j] -> heading) == 'S') {

                            $RobotListActual[$j] -> heading = 'W';
                            
                        }
                        
                    }

                    else {

                    }

                }

            }

            //dd($RobotListNext);

            for ($j = 0; $j < count($RobotListActual); $j++) {

                $hits = 0;

                if ($i >= count(str_split($RobotListActual[$j] -> commands))) {




                }

                else {

                    if ($RobotListNext[$j] -> x >= 0 && $RobotListNext[$j] -> x <= $Shop -> x && $RobotListNext[$j] -> y >= 0 && $RobotListNext[$j] -> y <= $Shop -> y) {

                            for ($k = 0; $k < count($RobotListActual); $k++) {

                                if ($j != $k) {

                                    $counter = $k;

                                    //echo($j . " / " . $k . " /// " . $RobotListNext[$j] -> robot_id . " / " . $RobotListNext[$j] -> x . " / " . $RobotListNext[$j] -> y . " / " . $RobotListNext[$k] -> robot_id . " / " . $RobotListNext[$k] -> x . " / " . $RobotListNext[$k] -> y . "<br>");

                                    if ($RobotListActual[$j] -> commands[$i] == "M") {

                                        if ($RobotListNext[$j] -> x == $RobotListNext[$k] -> x ) {

                                            if ($RobotListNext[$j] -> y == $RobotListNext[$k] -> y) {

                                                //$hits++;
                                                array_push($CollisionList1, $RobotListNext[$j] -> robot_id);
                                                array_push($CollisionList2, $RobotListNext[$k] -> robot_id);
                                                array_push($Collisions[$i], $RobotListNext[$j] -> robot_id);

                                                //echo($j . " / " . $k . " /// " . $RobotListNext[$j] -> robot_id . " / " . $RobotListNext[$j] -> x . " / " . $RobotListNext[$j] -> y . " / " . $RobotListNext[$k] -> robot_id . " / " . $RobotListNext[$k] -> x . " / " . $RobotListNext[$k] -> y . "<br>");

                                            }

                                        }

                                        if ($RobotListNext[$j] -> x == $RobotListActual[$k] -> x ) {

                                            if ($RobotListNext[$j] -> y == $RobotListActual[$k] -> y) {

                                                if ($RobotListActual[$j] -> heading == "N") {

                                                    if ($RobotListActual[$k] -> heading == "S") {

                                                        //$hits++;
                                                        array_push($CollisionList1, $RobotListNext[$j] -> robot_id);
                                                        array_push($CollisionList2, $RobotListNext[$k] -> robot_id);
                                                        array_push($Collisions[$i], $RobotListNext[$k] -> robot_id);

                                                    }

                                                }

                                                else if ($RobotListActual[$j] -> heading == "E") {

                                                    if ($RobotListActual[$k] -> heading == "W") {

                                                        //$hits++;
                                                        array_push($CollisionList1, $RobotListNext[$j] -> robot_id);
                                                        array_push($CollisionList2, $RobotListNext[$k] -> robot_id);
                                                        array_push($Collisions[$i], $RobotListNext[$k] -> robot_id);

                                                    }

                                                }

                                                else if ($RobotListActual[$j] -> heading == "W") {

                                                    if ($RobotListActual[$k] -> heading == "E") {

                                                        //$hits++;
                                                        array_push($CollisionList1, $RobotListNext[$j] -> robot_id);
                                                        array_push($CollisionList2, $RobotListNext[$k] -> robot_id);
                                                        array_push($Collisions[$i], $RobotListNext[$k] -> robot_id);

                                                    }

                                                }

                                                else if ($RobotListActual[$j] -> heading == "S") {

                                                    if ($RobotListActual[$k] -> heading == "N") {

                                                        //$hits++;
                                                        array_push($CollisionList1, $RobotListNext[$j] -> robot_id);
                                                        array_push($CollisionList2, $RobotListNext[$k] -> robot_id);
                                                        array_push($Collisions[$i], $RobotListNext[$k] -> robot_id);

                                                    }

                                                }

                                                if ($RobotListNext[$k] -> x >= 0 && $RobotListNext[$k] -> x <= $Shop -> x && $RobotListNext[$k] -> y >= 0 && $RobotListNext[$k] -> y <= $Shop -> y) {


                                                }

                                                else {

                                                    //$hits++;
                                                    array_push($CollisionList1, $RobotListNext[$j] -> robot_id);
                                                    array_push($CollisionList2, $RobotListNext[$k] -> robot_id);
                                                    array_push($Collisions[$i], $RobotListNext[$j] -> robot_id);
                                                    //echo("HERE" . "<br>");
                                                    //echo($j . " / " . $k . " /// " . $RobotListNext[$j] -> robot_id . " / " . $RobotListNext[$j] -> x . " / " . $RobotListNext[$j] -> y . " / " . $RobotListNext[$k] -> robot_id . " / " . $RobotListNext[$k] -> x . " / " . $RobotListNext[$k] -> y . "<br>");
                                                    //dd("HERE");

                                                    //echo("??????" . $j . " / " . $k . " /// " . $RobotListNext[$j] -> robot_id . " / " . $RobotListNext[$j] -> x . " / " . $RobotListNext[$j] -> y . " / " . $RobotListNext[$k] -> robot_id . " / " . $RobotListNext[$k] -> x . " / " . $RobotListNext[$k] -> y . "<br>");

                                                }

                                            }

                                        }

                                    }

                                }

                                //echo('Actualid:' . $RobotListActual[$j] -> robot_id . ' / ' . $RobotListActual[$j] -> x . ' /// ' . $RobotListActual[$j] -> y . '<br>');
                                //echo('Nextid:' . $RobotListNext[$j] -> robot_id . ' / ' . $RobotListNext[$j] -> x . ' /// ' . $RobotListNext[$j] -> y . '<br>');
                                $HitRobot = $RobotListActual[$k] -> robot_id;

                            }


                                //echo('Actualid:' . $RobotListActual[$j] -> robot_id . ' / ' . $RobotListActual[$j] -> x . ' /// ' . $RobotListActual[$j] -> y . '<br>');


                            if ($hits > 0) {

                                //$RobotListNext[$j] -> x = $RobotListActual[$j] -> x;
                                //$RobotListNext[$j] -> y = $RobotListActual[$j] -> y;

                                if ($RobotListNext[$counter] -> x >= 0 && $RobotListNext[$counter] -> x <= $Shop -> x && $RobotListNext[$counter] -> y >= 0 && $RobotListNext[$counter] -> y <= $Shop -> y) {

                                    //$RobotListNext[$counter] -> x = $RobotListActual[$counter] -> x;
                                    //$RobotListNext[$counter] -> y = $RobotListActual[$counter] -> y;
                                    array_push($Collisions[$i], $RobotListNext[$counter] -> robot_id);

                                    //dd("Here");

                                }

                                else {

                                }

                                    //array_push($message, "Robot Collision Imminent, Robot ID " . $RobotListActual[$j] -> robot_id . " and Robot ID " . $HitRobot . " almost collided with each other " . "... Terminating current move input.");
                                    //echo("        Robot Collision Imminent, Robot ID " . $RobotListActual[$j] -> robot_id . " almost collided with Robot ID " . $HitRobot . "... Terminating current move input." . " <br>");

                            }

                            else {

                                //$RobotListActual[$j] -> x = $RobotListNext[$j] -> x;
                                //$RobotListActual[$j] -> y = $RobotListNext[$j] -> y;

                                //array_push($CollisionList1, 0);
                                //array_push($CollisionList2, 0);
                                array_push($Collisions[$i], 0);

                            }

                                //echo('Actualid:' . $RobotListActual[$j] -> robot_id . ' / ' . $RobotListActual[$j] -> x . ' /// ' . $RobotListActual[$j] -> y . '<br>');
                                //echo('Nextid:' . $RobotListNext[$j] -> robot_id . ' / ' . $RobotListNext[$j] -> x . ' /// ' . $RobotListNext[$j] -> y . '<br>');                            

                        }

                        else {

                            if ($i >= count(str_split($RobotListActual[$j] -> commands))) {

                            }

                            else {

                                //$RobotListNext[$j] -> x = $RobotListActual[$j] -> x;
                                //$RobotListNext[$j] -> y = $RobotListActual[$j] -> y;
                                array_push($CollisionList1, $RobotListNext[$j] -> robot_id);
                                array_push($CollisionList2, 0);
                                array_push($Collisions[$i], $RobotListNext[$j] -> robot_id);
                                array_push($message, "Wall Collision imminent for " . $RobotListNext[$j] -> robot_id . " ... Terminating current move input.");
                                //echo("        Wall Collision" . "<br>");

                            }

                        }

                    }

                }

                $CollisionListFinal1 = array_values(array_unique($CollisionList1));
                $CollisionListFinal2 = array_values(array_unique($CollisionList2));

                //for ($b = 0; $b < count($Collisions); $b++) {

                    //$Collisions[$b];

                //}

                //echo($i);
                //dd($Collisions[$i]);

                //dd(array_unique($Collisions[$i]));

                $currentSet = array_values(array_unique($Collisions[$i]));

                //dd(array_values($currentSet));

                foreach($RobotListActual as $Robo) {

                    $hits = 0;

                    for($v = 0; $v <= count($currentSet) - 1; $v++) {

                        //dd($CollisionListFinal1);

                        //dd($Robo -> robot_id);

                        if($Robo -> robot_id == $currentSet[$v]) {

                            $hits++;

                        }


                        //echo( $Robo -> robot_id . ' / ' . $hits . '<br>');

                        //dd($CollisionListFinal1[$v]);

                    }

                    //dd("Here");

                    //dd($CollisionListFinal1);

                    //echo( $Robo -> robot_id . ' / ' . $hits . '<br>');

                    if ($hits == 0) {

                        for($z = 0; $z < count($RobotListNext); $z++) {
                            //dd($RobotListNext);

                            if ($Robo -> robot_id == $RobotListNext[$z] -> robot_id) {

                                
                                //echo($Robo -> robot_id . '<br>');
                                //echo( $Robo -> robot_id . ' / ' . $hits . '<br>');

                                //echo($Robo -> robot_id . ': ' . $Robo -> x . '/' . $Robo -> y . ' /// ' . $RobotListNext[$z] -> x . '/' . $RobotListNext[$z] -> y . '<br>');
                                if ($RobotListNext[$z] -> x >= 0 && $RobotListNext[$z] -> x <= $Shop -> x && $RobotListNext[$z] -> y >= 0 && $RobotListNext[$z] -> y <= $Shop -> y) {
                                    $Robo -> x = $RobotListNext[$z] -> x;
                                    $Robo -> y = $RobotListNext[$z] -> y;
                                }

                                else {

                                    //echo("        Wall Collision imminent, Robot ID: " . $Robo -> robot_id . "<br>");

                                }

                            }

                        }

                    }

                    else {

                        for($z = 0; $z < count($RobotListNext); $z++) {
                            //dd($RobotListNext);

                            if ($Robo -> robot_id == $RobotListNext[$z] -> robot_id) {

                                
                                //echo($Robo -> robot_id . '<br>');
                                //echo( $Robo -> robot_id . ' / ' . $hits . '<br>');

                                //echo($Robo -> robot_id . ': ' . $Robo -> x . '/' . $Robo -> y . ' /// ' . $RobotListNext[$z] -> x . '/' . $RobotListNext[$z] -> y . '<br>');

                                if ($RobotListNext[$z] -> x >= 0 && $RobotListNext[$z] -> x <= $Shop -> x && $RobotListNext[$z] -> y >= 0 && $RobotListNext[$z] -> y <= $Shop -> y) {

                                    array_push($message, "Robot Collision Imminent, Robot ID " . $Robo -> robot_id . " almost hit another " . "... Terminating current move input.");

                                }

                                else {


                                }

                                $RobotListNext[$z] -> x = $Robo -> x;
                                $RobotListNext[$z] -> y = $Robo -> y;

                            }

                        }

                    }

                }

        }

        $FilteredResults = (array) $Shop;

        if ($FilteredResults != null) {

            $data = array ('id' => $Shop->id, 'x' => $Shop->x, 'y' => $Shop->y, 'owner' => $RobotListActual, 'message' => $message);
                
            return view('simulateGlobal') -> with ($data);

        }

    }

}
