<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table = 'shopunits';
	protected $fillable = ['id', 'width', 'height'];
	public $timestamps = false;
}
