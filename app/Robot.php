<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Robot extends Model
{

	protected $table = 'robotunits';
	protected $fillable = ['robot_id', 'owner', 'x', 'y', 'heading', 'commands', 'date'];
	public $timestamps = false;

}
