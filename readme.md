This is my Take Home Exam for Hey You.

I have titled the app Firefly.

Languages used:

Back End: Php and Laravel
Front End: HTML/CSS/JavaScript and jQuery

You can visit the temporary hosted version of this app at:

## http://firefly.krayap.com/ ##


Control panel has 5 different options:

1. Create Shop:
2. Edit Existing Shop:
3. Delete Existing Shop:
4. Simulate Existing Shop:
5. Run Local Simulation:

### Create Shop: ### 

Creates a shop. Input X and Y dimensions.

### Edit Existing Shop: ### 

Allows you to Create, Edit and Delete Robots assigned to a shop. Make sure that the robots do not go beyond the bounds of the shop's walls.
### 
Delete Existing Shop: ###

Removes a Shop from the database.

### Simulate Existing Shop: ###

Displays the initial state of a Shop and all the Robots inside it. Upon hitting the "Execute Simulation" button, the Robots will execute all their actions until all actions have been exhausted. This feature has been tested.

* Tested for head on collisions with Robots one space apart - ok Please see Shop ID 6. 
* Tested for head on collisions with robots next to each other - ok Please see Shop 8. 
* Tested for lateral collisions, robot colliding with the side of another robot/back of another robot - ok Please see Shop ID 10. 
* Tested for collisions between robots approaching another robot about to collide with the wall - ok Please see Shop ID 11. 
* Tested if robots adjacent to one another moving in the same direction will collide if both moving continuously - no collisions - ok Please see Shop ID 13 . 
* Tested for collisions if robot is next to another robot and will hit it next turn, but the robot it will supposedly hit will move out of the way next turn as well - no collisions - ok Please see Shop ID 17. 
* Tested 4 way collision - all robots collided at the same time - ok Please see Shop ID 18.
* Tested 3 way collision next to wall with middle robot colliding with wall - all robots collided at the same time - ok Please see Shop ID 20.

### Run Local Simulation: ###

Displays a form that allows you to input the shop's width and height right off the bat and then add robots straight to it. Upon hitting "Run Simulation", each Robot inputted in the file will execute each of their actions until all actions have been exhausted. (Known bugs: Form Validation will still need to disallow 2 robots from occupying the same space. This is only during placement of robots. This has been corrected in the global online simulation in "Simulate Existing Shop"... Also, due to time constraints the robot positions in this feature is restricted to single digit numbers. "x" = 0-9 and "y" = 0-9. This feature can easily be corrected given just a few more hours).