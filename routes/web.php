<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*

Route::get('/', function () {
    return view('welcome');
});

*/


Route::get('basic', function () {
    return view('basic');
});

Route::get('/', 'Controller@control_panel');

Route::get('control', 'Controller@control_panel');

Route::get('createShop', 'Controller@creating_shop');

Route::post('createShop', 'Controller@createShop');

Route::delete('deleteShop', 'Controller@deleteShop');

Route::get('editShop', 'Controller@editing_Shop');

Route::post('createRobot', 'Controller@createRobot');

Route::delete('editShop', 'Controller@deleteRobot');

Route::put('editShop', 'Controller@editRobot');

Route::get('editRobot', 'Controller@control_panel');

Route::get('deleteRobot', 'Controller@control_panel');

Route::get('simulateGlobal', 'Controller@simulate_Global');

Route::post('simulateGlobal', 'Controller@execute_GlobalSimulation');

