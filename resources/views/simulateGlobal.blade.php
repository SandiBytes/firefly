<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">

        <title>Firefly</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>


        <div class="field">

            <div id="left" class="full-height">

                    <div class="leftboxtop">

                        <h2 class="mainlabel">Shop Statistics</h2>
                        <div class="line2">
                        </div>

                    </div>

                    <div class="leftbox">

                        <p>Shop ID: {!!Form::label('id', $id)!!}</p>
                        <p>Shop width: {!!Form::label('x', $x)!!}</p>
                        <p>Shop height: {!!Form::label('y', $y)!!}</p>

                        <div class="line2">
                        </div>

                    </div>

                    <div class="leftbox">

                        {!! Form::open(array('method' => 'post', 'action' => 'Controller@execute_GlobalSimulation')) !!}
                        {!! Form::hidden('id', $id) !!}
                        {!! Form::submit('Execute Simulation', ['class' => 'btn btn-success']) !!}
                        {!! Form::close() !!}

                        <br>

                        <div class="cells"><a id="back" href="control" class="btn btn-warning">Back</a></div>



                    </div>


            </div>

            <div class="contents">

                <div id="top">

                    <div class="title m-b-md">
                        Firefly
                        <div id="line">
                        </div>
                    </div>

                </div>

                <div id="initial">

                    <div id="robotstats">

                        <div id="robotstats-labels">

                            <h2 class="mainlabel">Robot Positions:</h2>

                        </div>

                        <div id="robotstats-container">

                            <div id="robotstats-content">

                                <br>

                                @foreach ($owner as $data)
                                <div class="row-info">
                                        <div class="cell-smaller"><p class="darktext">id: {!!Form::label($data -> robot_id)!!}</p></div>
                                        <div class="cell-small"><p class="darktext">x-location: {!!Form::label($data -> x)!!}</p></div>
                                        <div class="cell-small"><p class="darktext">y-location: {!!Form::label($data -> y)!!}</p></div>
                                        <div class="cell-small"><p class="darktext">heading: {!!Form::label($data -> heading)!!}</p></div>
                                        <div class="cell-large"><p class="darktext">commands: {!!Form::label($data -> commands)!!}</p></div>
                                </div>
                                @endforeach

                            </div>

                        </div>

                    </div>

                    <div id="logbox">

                        <div id="logs-labels">

                            <h2 class="mainlabel">Logs</h2>

                        </div>

                        <div id="logbox-container">

                            <div id="logbox-contents">

                                @foreach ($message as $data)

                                <div class="logbox-content">
                                <p class="darktext">{!! $data !!}</p>
                                </div>

                                @endforeach

                            </div>

                        </div>

                    </div>


                </div>

            </div>

        </div>
        <div id="overlayOuter" class="flex-center overlay">

                <div id="overlayInner" class="modaline">

                </div>

        </div>
    </body>

    <script async>

        $(document).ready( function () {

            $(".button").click( function () {
                document.getElementById("overlayOuter").style.zIndex = 1;
                document.getElementById("overlayOuter").style.opacity= 0.2;
            });

        });


    </script>


</html>
