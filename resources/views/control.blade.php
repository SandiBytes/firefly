<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ URL::asset('js/jquery-3.2.0.min.js') }}"></script>

        <title>Firefly</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">

                <div id="main-menu-top">

                    <div class="title m-b-md">
                        Firefly
                    </div>


                </div>

                <div id="main-menu-bottom">

                    <div class="links">
                        <a class="cutelink" href="createShop">Create New Shop</a>
                        <a class="cutelink" onclick="Initialize(1)">Edit Existing Shop</a>
                        <a class="cutelink" onclick="Initialize(2)">Delete Existing Shop</a>
                        <a class="cutelink" onclick="Initialize(3)">Simulate Existing Shop</a>
                        <a class="cutelink" href="basic">Run Local Simulation</a>
                    </div>
                    <br>
                    {!!Form::label('data', $alert, ['class' => 'lighttext ShiftUpper'])!!}

                </div>
                <div id="fancybox">
                    <div id="fancyline">
                    </div>
                </div>

            </div>
        </div>

        <div id="overlayOuter" class="flex-center overlay" onclick="Initialize(1)">
        </div>

        <div id="overlayInner1" class="modaline">
            {!! Form::open(array('method' => 'get', 'action' => 'Controller@editing_Shop', 'class' => 'Divider')) !!}
            <h1 class="Division ShiftUp fontBoost2">Edit Shop</h1>
            {!! Form::text('id', null, array('class' => 'form-control Division SizeBoost fontBoost1')) !!}
            {!! Form::submit('Select ID', ['class' => 'btn btn-lg btn-info Division fontBoost1']) !!}
            {!! Form::close() !!}
        </div>

        <div id="overlayInner2" class="modaline">
            {!! Form::open(array('method' => 'delete', 'action' => ['Controller@deleteShop'], 'class' => 'Divider')) !!}
            <h1 class="Division ShiftUp fontBoost2">Delete Shop</h1>
            {!! Form::text('id', null, array('class' => 'form-control Division SizeBoost fontBoost1')) !!}
            {!! Form::submit('Delete ID', ['class' => 'btn btn-lg btn-warning Division fontBoost1']) !!}
            {!! Form::close() !!}
        </div>

        <div id="overlayInner3" class="modaline">
            {!! Form::open(array('method' => 'get', 'action' => ['Controller@simulate_Global'], 'class' => 'Divider')) !!}
            <h1 class="Division ShiftUp fontBoost2">Simulate Shop</h1>
            {!! Form::text('id', null, array('class' => 'form-control Division SizeBoost fontBoost1')) !!}
            {!! Form::submit('Select ID', ['class' => 'btn btn-lg btn-info Division fontBoost1']) !!}
            {!! Form::close() !!}
        </div>

    </body>

    <script async>

    var Visible = false;

    $(document).ready( Execute );

    function Execute() {
 
        setTimeout(Transform, 100);

    }

    function Transform() {

        $("#main-menu-top").css("padding-top", "25vh");
        $("#fancyline").css("width", "100%");
        setTimeout(Transform2, 1000);

    }

    function Transform2() {

        $("#main-menu-top").css("background-color", "#0080ff");
        $("#main-menu-bottom").css("background-color", "#ac00e6");

    }

    function Initialize(i) {

        $('body').css("overflow-y", "hidden");
        modals = document.getElementsByClassName('modaline');

        if (i == 1) {

            if (Visible == false) {

            document.getElementById('overlayOuter').style.display = "flex";
            document.getElementById('overlayInner1').style.display = "flex";
            modals[0].style.display = "flex";

            Visible = true;
            setTimeout( function() { Open(i) }, 100);

            }

            else {

            document.getElementById('overlayOuter').style.opacity = "0";
            document.getElementById('overlayInner1').style.opacity = "0";
            document.getElementById('overlayInner2').style.opacity = "0";
            document.getElementById('overlayInner3').style.opacity = "0";
            Visible = false;
            setTimeout(Close, 500);

            }
        }

        else if (i == 2) {

            if (Visible == false) {

            document.getElementById('overlayOuter').style.display = "flex";
            document.getElementById('overlayInner2').style.display = "flex";
            modals[1].style.display = "flex";
            Visible = true;
            setTimeout( function() { Open(i) }, 100);

            }
            else {

            document.getElementById('overlayOuter').style.opacity = "0";
            document.getElementById('overlayInner1').style.opacity = "0";
            document.getElementById('overlayInner2').style.opacity = "0";
            document.getElementById('overlayInner3').style.opacity = "0";
            Visible = false;
            setTimeout(Close, 500);

            }

        }

        else if (i == 3) {

            if (Visible == false) {

            document.getElementById('overlayOuter').style.display = "flex";
            document.getElementById('overlayInner3').style.display = "flex";
            modals[2].style.display = "flex";
            Visible = true;
            setTimeout( function() { Open(i) }, 100);

            }
            else {

            document.getElementById('overlayOuter').style.opacity = "0";
            document.getElementById('overlayInner1').style.opacity = "0";
            document.getElementById('overlayInner2').style.opacity = "0";
            document.getElementById('overlayInner3').style.opacity = "0";
            Visible = false;
            setTimeout(Close, 500);

            }

        }
    }

    function Open(j) {

        if (j == 1) {

        document.getElementById('overlayOuter').style.opacity = "0.6";
        document.getElementById('overlayInner1').style.opacity = "1";

        }

        else if (j == 2) {

        document.getElementById('overlayOuter').style.opacity = "0.6";
        document.getElementById('overlayInner2').style.opacity = "1";

        }

        else if (j == 3) {

        document.getElementById('overlayOuter').style.opacity = "0.6";
        document.getElementById('overlayInner3').style.opacity = "1";

        }

    }

    function Close(j) {

        document.getElementById('overlayOuter').style.display = "none";
        document.getElementById('overlayInner1').style.display = "none";
        document.getElementById('overlayInner2').style.display = "none";
        document.getElementById('overlayInner3').style.display = "none";
        modals[0].style.display = "none";
        modals[1].style.display = "none";
        modals[2].style.display = "none";

    }

    </script>
</html>
