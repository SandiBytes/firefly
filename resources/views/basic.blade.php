<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ URL::asset('js/jquery-3.2.0.min.js') }}"></script>

        <title>Firefly</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="field position-ref full-height">

            <div class="content">

                <div id="left" class="full-height">

                <div class="leftboxtop">

                        <h2 class="mainlabel">Basic Simulation</h2>
                        <div class="line2">
                        </div>

                </div>

                <div class="leftbox">

                        <p>This is a Protype, being a prototype, only 1 digit size shops are allowed. width 1-9 and height 1-9</p>

                        <p>Enter the robot positions in the following format: x position, y position, heading (x = 1-9, y = 1-9 and heading = N,E,W,S)</p>

                        <p>For the commands, enter either M to move forware, L to turn Left and R to turn Right</p>
                        <div class="line2">
                        </div>

                </div>

                <div class="leftbox">

                    <div class="cells"><a id="back" href="control" class="btn btn-warning">Back</a></div>

                </div>

                </div>

                <div id="basicScreen">

                <div id="top">

                    <div class="title m-b-md">
                        Firefly
                        <div id="line">
                        </div>
                    </div>

                </div>

                <br>
                <br>
                <br>

                <br>
                <br>
                <br>

                <br>
                <br>
                <br>

                <br>
                <br>
                <br>

                <br>
                <br>
                <br>

                <div class="links">
                    <input id="shop_x" placeholder="Enter Shop Width: " maxlength="1" class="fontBoost3">
                    <input id="shop_y" placeholder="Enter Shop Height: " maxlength="1" class="fontBoost3">
                    <input type="submit" value="Submit" onclick="InputDimensions()" class="btn btn-info fontboost1">
                </div>

                <br>

                <button id="CreateRobot" type="button" class="btn btn-info fontboost1">Create a new Robot</button>

                <br>
                <br>

                <div class="Robots">
                    <div id="0">
                    <textarea class="RobotPosition fontBoost3" placeholder="X position, Y Position then One of Either N,E,W,S" cols="50" rows="1"></textarea>
                    <textarea class="RobotCommands fontBoost3" placeholder="M, L or R Only" cols="50" rows="1"></textarea>
                    </div>
                </div>

                <br>

                <button id="postbutton" type="button" onclick="Simulate()" class="btn btn-lg btn-success fontboost2">Run Simulation</button>

                </div>

            </div>
        </div>
    </body>

    <script>

    $(document).ready( Execute );

    var BotCoverID = 0;

    var Bots = [];

    var shop_width;
    var shop_height;

    function Execute() {
 
        $("#CreateRobot").click( Create );

    }

    function InputDimensions (X, Y) {

        shop_width = document.getElementById("shop_x").value;
        shop_height = document.getElementById("shop_y").value;

    }

    function Create() {

        BotCoverID++;

        $(".Robots").append("<div id=" +BotCoverID+ "><textarea class='RobotPosition fontBoost3' placeholder='X position, Y Position then One of Either N,E,W,S' cols='50' rows='1'></textarea><span> </span><textarea class='RobotCommands fontBoost3' placeholder='M, L or R Only' cols='50' rows='1'></textarea></div>");
        //document.getElementById(BotCoverID).childNodes[0].value = "4";
        //document.getElementById(BotCoverID).childNodes[2].value = "6";

        //alert(document.getElementById("1").childNodes[0].value);

    }

    function Simulate() {

        var hits = 0;
        var zero = 0;
        var negative = -1;

        shop_width = document.getElementById("shop_x").value;
        shop_height = document.getElementById("shop_y").value;

        var xcurrent = [];
        var ycurrent = [];
        var xnext = [];
        var ynext = [];
        var headings = [];
        var commandings = [];

        var collisions = [[0]];
        //alert(collisions);

        //collisions[0].push(1);
        //collisions[0].push(2);

        //alert(collisions);
        //alert(collisions[0][1]);

        //alert(BotCoverID);
        if (shop_width == "" || shop_height == "") {

            if (shop_width == "") {

                document.getElementById("shop_x").style.backgroundColor = "#ADD8E6";

            }

            else {

                document.getElementById("shop_x").style.backgroundColor = "#FFFFFF";

            }

            if (shop_height == "") {

            document.getElementById("shop_y").style.backgroundColor = "#ADD8E6";

            }

            else {

                document.getElementById("shop_y").style.backgroundColor = "#FFFFFF";
                
            }

        }

        else {

            if (isNaN(shop_width) || isNaN(shop_height)) {

                if(isNaN(shop_width)) {

                    document.getElementById("shop_x").style.backgroundColor = "#ADD8E6";

                }

                else {

                    document.getElementById("shop_x").style.backgroundColor = "#FFFFFF";

                }

                if(isNaN(shop_height)) {

                    document.getElementById("shop_y").style.backgroundColor = "#ADD8E6";

                }

                else {

                    document.getElementById("shop_y").style.backgroundColor = "#FFFFFF";

                }

            }

            else {

            shop_width = document.getElementById("shop_x").value;
            shop_height = document.getElementById("shop_y").value;

            document.getElementById("shop_x").style.backgroundColor = "#FFFFFF";
            document.getElementById("shop_y").style.backgroundColor = "#FFFFFF";

                            for (i = 0; i < BotCoverID + 1; i++) {

                                if (i == 0) {

                                    if (document.getElementById(i).childNodes[1].value == "") {

                                        document.getElementById(i).childNodes[1].style.backgroundColor = "#ADD8E6";
                                        hits++;
                                    }

                                    else {

                                        var latter = document.getElementById(i).childNodes[1].value.replace(/ /g, '').toUpperCase();

                                         document.getElementById(i).childNodes[1].value = latter;

                                        var initializer = latter.split('');

                                        if (initializer.length > 3) {

                                            document.getElementById(i).childNodes[1].style.backgroundColor = "#ADD8E6";
                                            hits++;

                                        }

                                        else {

                                            if (isNaN(initializer[0]) || isNaN(initializer[1])) {

                                                document.getElementById(i).childNodes[1].style.backgroundColor = "#ADD8E6";
                                                hits++;

                                            }

                                            else {

                                                //alert("Correct!!!");
                                                //document.getElementById(i).childNodes[1].style.backgroundColor = "white";

                                                if(initializer[2] == "N" || initializer[2] == "E" || initializer[2] == "W" || initializer[2] == "S") {

                                                    document.getElementById(i).childNodes[1].style.backgroundColor = "#FFFFFF";
                                                    xcurrent[i] = initializer[0];
                                                    ycurrent[i] = initializer[1];
                                                    xnext[i] = initializer[0];
                                                    ynext[i] = initializer[1];
                                                    headings[i] = initializer[2];


                                                }

                                                else {

                                                    document.getElementById(i).childNodes[1].style.backgroundColor = "#ADD8E6";
                                                    hits++;

                                                }

                                            }

                                        }

                                        //var initial = document.getElementById(i).childNodes[1].value = str.split(" ");

                                        //alert(initial);

                                    }

                                    if (document.getElementById(i).childNodes[3].value == "") {

                                        document.getElementById(i).childNodes[3].style.backgroundColor = "#ADD8E6";
                                        hits++;

                                    }

                                    else {

                                        var latter = document.getElementById(i).childNodes[3].value.replace(/ /g, '').toUpperCase();

                                        var commands = latter.split('');

                                        if (commands.length > 30) {

                                            document.getElementById(i).childNodes[3].style.backgroundColor = "#ADD8E6";
                                            hits++;

                                        }

                                        else {

                                            for(v = 0; v < commands.length; v++) {

                                                if(commands[v] == "M" || commands[v] == "L" || commands[v] == "R") {

                                                    commandings[i] = commands;

                                                    document.getElementById(i).childNodes[3].style.backgroundColor = "#FFFFFF";

                                                }

                                                else {

                                                    document.getElementById(i).childNodes[3].style.backgroundColor = "#ADD8E6";
                                                    hits++;

                                                }

                                            }

                                        }

                                    }

                                } // if i == 0

                                else {

                                    if (document.getElementById(i).childNodes[0].value == "") {

                                    document.getElementById(i).childNodes[0].style.backgroundColor = "#ADD8E6";
                                    hits++;

                                    }

                                    else {

                                        var latter = document.getElementById(i).childNodes[0].value.replace(/ /g, '').toUpperCase();

                                        document.getElementById(i).childNodes[0].value = latter;

                                        var initializer = latter.split('');

                                        if (initializer.length > 3) {

                                            document.getElementById(i).childNodes[0].style.backgroundColor = "#ADD8E6";
                                            hits++;

                                        }

                                        else {

                                            if (isNaN(initializer[0]) || isNaN(initializer[1])) {

                                                document.getElementById(i).childNodes[0].style.backgroundColor = "#ADD8E6";
                                                hits++;

                                            }

                                            else {

                                                //alert("Correct!!!");
                                                //document.getElementById(i).childNodes[0].style.backgroundColor = "white";

                                                if(initializer[2] == "N" || initializer[2] == "E" || initializer[2] == "W" || initializer[2] == "S") {

                                                    document.getElementById(i).childNodes[0].style.backgroundColor = "#FFFFFF";
                                                    xcurrent[i] = initializer[0];
                                                    ycurrent[i] = initializer[1];
                                                    xnext[i] = initializer[0];
                                                    ynext[i] = initializer[1];
                                                    headings[i] = initializer[2];

                                                }

                                                else {

                                                    document.getElementById(i).childNodes[0].style.backgroundColor = "#ADD8E6";
                                                    hits++;

                                                }

                                            }

                                        }

                                        //var initial = document.getElementById(i).childNodes[1].value = str.split(" ");

                                        //alert(initial);

                                    }

                                    if (document.getElementById(i).childNodes[2].value == "") {

                                    document.getElementById(i).childNodes[2].style.backgroundColor = "#ADD8E6";
                                    hits++;

                                    }

                                    else {

                                        var latter = document.getElementById(i).childNodes[2].value.replace(/ /g, '').toUpperCase();

                                        var commands = latter.split('');

                                        if (commands.length > 30) {

                                            document.getElementById(i).childNodes[2].style.backgroundColor = "#ADD8E6";
                                            hits++;

                                        }

                                        else {

                                            for(v = 0; v < commands.length; v++) {

                                                commandings[i] = commands;

                                                if(commands[v] == "M" || commands[v] == "L" || commands[v] == "R") {

                                                    document.getElementById(i).childNodes[2].style.backgroundColor = "#FFFFFF";

                                                }

                                                else {

                                                    document.getElementById(i).childNodes[2].style.backgroundColor = "#ADD8E6";
                                                    hits++;

                                                }

                                            }

                                        }

                                    }

                                } //if i != 0

                        } //For Loop  for checking input Ends Here

                        if(hits == 0) { // checks if every input is in place

                            var highest = 0;

                            for (a = 0; a < BotCoverID + 1; a++) { //checks for longest command input

                                if (a == 0) {

                                    highest = document.getElementById(a).childNodes[3].value.length;

                                }

                                else {

                                    if (a < BotCoverID + 1) {

                                        if(document.getElementById(a).childNodes[2].value.length > highest) {

                                            highest = document.getElementById(a).childNodes[2].value.length;

                                            //alert(highest1);

                                        }

                                    }

                                }

                            }

                            //alert(BotCoverID + 1);

                        for (z = 1; z < BotCoverID + 1; z++) {

                            collisions.push(z);
                            
                        }

                        var CollisionBox = new Array(highest);

                        for (y = 0; y < highest + 1; y++) {

                            CollisionBox[y] = new Array();
                            
                        }

                        //console.log(CollisionBox)


                        //alert(CollisionBox[0].length);

                        //alert(collisions);

                        for (b = 0; b < highest; b++) { // will now formally run master loop ($i) in other file

                            for (c = 0; c < BotCoverID + 1; c++) { //will run through each bot

                                if (b >= commandings[c].length) {


                                }

                                else {

                                    if(commandings[c][b] == 'M') {

                                        //alert(commandings[c][b]);

                                        //if ((document.getElementById(c).childNodes[2].value) == 'N' || (document.getElementById(c).childNodes[2].value) == 'n') {
                                        if (headings[c] == 'N') {

                                            //alert(document.getElementById(c).childNodes[2].value);
                                            adder = ycurrent[c];
                                            adder--;
                                            ynext[c] = adder;
                                            //alert('n');

                                        }

                                        //else if ((document.getElementById(c).childNodes[2].value) == 'E' || (document.getElementById(c).childNodes[2].value) == 'e') {
                                        else if (headings[c] == 'E') {

                                            //alert(document.getElementById(c).childNodes[2].value);
                                            adder = xcurrent[c];
                                            adder++;
                                            xnext[c] = adder;
                                            //alert('e');

                                        }

                                        //else if ((document.getElementById(c).childNodes[2].value) == 'W' || (document.getElementById(c).childNodes[2].value) == 'w') {
                                        else if (headings[c] == 'W') {

                                            //alert(document.getElementById(c).childNodes[2].value);
                                            adder = xcurrent[c];
                                            adder--;
                                            xnext[c] = adder;
                                            //alert('e');

                                        }

                                        //else if ((document.getElementById(c).childNodes[2].value) == 'S' || (document.getElementById(c).childNodes[2].value) == 's') {
                                        else if (headings[c] == 'S') {

                                            //alert(document.getElementById(c).childNodes[2].value);

                                            adder = ycurrent[c];
                                            adder++;
                                            ynext[c] = adder;
                                            //alert(ynext[c]);

                                        }

                                    }

                                    else if (commandings[c][b] == 'L') {

                                        if (headings[c] == 'N') {

                                            headings[c] = 'W';

                                        }

                                        else if (headings[c] == 'E') {

                                            headings[c] = 'N';

                                        }

                                        else if (headings[c] == 'W') {

                                            headings[c] = 'S';

                                        }

                                        else if (headings[c] == 'S') {

                                            headings[c] = 'E';

                                        }

                                    }

                                    else if (commandings[c][b] == 'R') {

                                        if (headings[c] == 'N') {

                                            headings[c] = 'E';

                                        }

                                        else if (headings[c] == 'E') {

                                            headings[c] = 'S';

                                        }

                                        else if (headings[c] == 'W') {

                                            headings[c] = 'N';

                                        }

                                        else if (headings[c] == 'S') {

                                            headings[c] = 'W';

                                        }

                                    } //else if move direction

                                } // check move list

                                //xcurrent[c] = xnext[c]; //test
                                //ycurrent[c] = ynext[c]; //test

                            } // initial loop for plotting next moves

                            for (c = 0; c < BotCoverID + 1; c++) { // cue second loop for checking collisions

                                hits = 0;

                                if (b >= commandings[c].length) { // check move list


                                }

                                else { // check move list

                                    if (xnext[c] >= 0 && xnext[c] <= shop_width && ynext[c] >= 0 && ynext[c] <= shop_height) { // check if outside wall

                                        for (d = 0; d < BotCoverID + 1; d++) { // loop a second time to check each robot against another

                                            //collisions[b].push(zero);
                                            //alert(collisions);

                                            if(c != d) { //make sure we are not comparing to self

                                                counter = d;

                                                //collisions[b].push(zero);
                                                //alert(collisions);

                                                if (commandings[c][b] == 'M') { //check if moving forward

                                                    if (xnext[c] == xnext[d]) { // check if next spot hits the spot of moving target

                                                        if (ynext[c] == ynext[d]) {

                                                            CollisionBox[b].push(c);

                                                            //alert(b);
                                                            /*
                                                            if(c == 0) {

                                                                collisions[b].push(zero);

                                                            }

                                                            else {

                                                                collisions[b].push(c);

                                                            }
                                                            */

                                                        }

                                                    } // check if next spot hits the spot of moving target

                                                    if (xnext[c] == xcurrent[d]) { // check if next spot hits the spot of adjacent targets moving towards each other

                                                        if (ynext[c] == ycurrent[d]) {

                                                            if(headings[c] = 'N') { // They have to be moving in opposite direction

                                                                if(headings[d] = 'S') {

                                                                    CollisionBox[b].push(c);
                                                                    CollisionBox[b].push(d);
                                                                    alert("here2");

                                                                }

                                                            }

                                                            else if(headings[c] = 'E') { // They have to be moving in opposite direction

                                                                if(headings[d] = 'W') {

                                                                    CollisionBox[b].push(c);
                                                                    CollisionBox[b].push(d);
                                                                    alert("here3");

                                                                }

                                                            }

                                                            else if(headings[c] = 'W') { // They have to be moving in opposite direction

                                                                if(headings[d] = 'E') {

                                                                    CollisionBox[b].push(c);
                                                                    CollisionBox[b].push(d);
                                                                    alert("here4");

                                                                }

                                                            }

                                                            else if(headings[c] = 'S') { // They have to be moving in opposite direction

                                                                if(headings[d] = 'N') {

                                                                    CollisionBox[b].push(c);
                                                                    CollisionBox[b].push(d);
                                                                    alert("here5");

                                                                }

                                                            }

                                                            if (xnext[d] >= 0 && xnext[d] <= shop_width && ynext[d] >= 0 && ynext[d] <= shop_height) { // check if target is colliding with wall



                                                            }

                                                            else {

                                                                CollisionBox[b].push(c); //ensures that collision will be detected even if target is about to hit wall

                                                                alert("here6");
                                                            }

                                                        }

                                                    } // check if next spot hits the spot of adjacent targets moving towards each other


                                                } //M

                                                else {

                                                    //CollisionBox[b].push(negative);
                                                    //CollisionBox[b].push(negative);


                                                }

                                            } // Compare with self

                                            //var HitRobot = $RobotListActual[$k] -> robot_id;


                                        } // Compare with others ($k inother file)

                                        /*
                                        if (hits > 0) {

                                            if (xnext[counter] >= 0 && xnext[counter] <= shop_width && ynext[counter] >= 0 && ynext[counter] <= shop_height) { // check if target is colliding with wall

                                                //collisions[b].push(counter);
                                                CollisionBox[b].push(counter);
                                                alert("here7");

                                            }

                                            else {

                                            }



                                        }

                                        else {

                                        //collisions[b].push(negative);
                                        //CollisionBox[b].push(negative);
                                        //alert("here8");
                                        //alert("here8");

                                        }
                                        */


                                    } // check if outside wall

                                    else { // check if outside wall

                                        if (b >= commandings[c].length) { // check move list


                                        }

                                        else {

                                            //CollisionBox[b].push(c);

                                        }

                                    } // check if outside wall

                                } // check move list

                            } // end second loop

                            //alert(collisions[0][0]);

                            //CollisionBox[b].push(c);

                            //var unique = CollisionBox[b].filter( Uniquer );
                            //alert(CollisionBox);

                            //alert(CollisionBox[b]);


                            //if (b >= commandings[c].length) {

                            //}

                            //else {

                            //}

                            //alert(CollisionBox[b]);

                            //alert(CollisionBox[b].length);

                            //for(w = 0; w < CollisionBox[b].length; w++) {

                                //alert(CollisionBox[b]);

                            //}

                            //alert(headings);

                            /*

                            if (CollisionBox[b] == null){

                                var currentSet = [];

                            }

                            else {

                                var cleaner = CollisionBox[b].filter(function(n){ return n != undefined });
                                var currentSet = cleaner.filter( Uniquer );
                                //var currentSet = CollisionBox[b].filter( Uniquer );

                            }


                            */

                            var cleaner = CollisionBox[b].filter(function(n){ return n != undefined });
                            var currentSet = cleaner.filter( Uniquer );

                            //var currentSet = CollisionBox[b];

                                for (e = 0; e <= BotCoverID; e++) { //final loop in master

                                    //console.log(commandings[e] + " " + b);

                                    var currentMoveSet = commandings[e];

                                    var actualHits = 0;

                                    for(f = 0; f < currentSet.length; f++) {

                                        if(e == currentSet[f]) {

                                            actualHits++;

                                        }

                                    }

                                    if (actualHits == 0) {

                                        //console.log(b);

                                        //console.log(commandings[e] + " / " + b);

                                        if(commandings[e][b] == 'M') {

                                            if (xnext[e] >= 0 && xnext[e] <= shop_width && ynext[e] >= 0 && ynext[e] <= shop_height) {

                                            xcurrent[e] = xnext[e];
                                            ycurrent[e] = ynext[e];

                                            }

                                            else {

                                                alert("Wall Collision Averted: " + "Robot " + e + " almost hit the wall at: " + xnext[e] + "/" + ynext[e]);
                                                
                                            }


                                        }

                                        else {

                                        xnext[e] = xcurrent[e];
                                        ynext[e] = ycurrent[e];

                                        }

                                    }

                                    else {

                                        alert("Robot Collision Averted: " + "Robot " + e + " almost hit another Robot at: " + xnext[e] + "/" + ynext[e]);

                                    }

                                    /*

                                    if (actualHits == 0) { //if no hits

                                        for(g = 0; g <= BotCoverID + 1; g++) {

                                            if (e == g) {

                                                if (xnext[e] >= 0 && xnext[e] <= shop_width && ynext[e] >= 0 && ynext[e] <= shop_height) {

                                                    //if(headings[e] == 'M') {

                                                    xcurrent[e] = xnext[e];
                                                    ycurrent[e] = ynext[e];

                                                    //}

                                                }

                                                else {

                                                }

                                            }

                                        }

                                    }

                                    else {

                                        for(g = 0; g <= BotCoverID + 1; g++) {

                                            if (e == g) {

                                                if (xnext[g] >= 0 && xnext[g] <= shop_width && ynext[g] >= 0 && ynext[g] <= shop_height) {

                                                    //ROBOT COLLISION

                                                //xnext[e] = xcurrent[e];
                                                //ynext[e] = xcurrent[e];


                                                }

                                                else {

                                                }

                                                xnext[e] = xcurrent[e];
                                                ynext[e] = xcurrent[e];


                                            }

                                        }

                                    }

                                    */


                                } //end final loop in master


                        } //end master loop

                        for(v = 0; v <= BotCoverID; v++) {

                            alert("Robot " + v + " Final Position: " +  xcurrent[v] + " / " + ycurrent[v] + " " + headings[v]);

                        }


                        //var unique = CollisionBox[0].filter( Uniquer );
                        //var currentset = unique.filter(function(n){ return n != undefined });

                        //alert(currentset);

                        //alert(CollisionBox[0].filter(function(n){ return n != undefined })); 

                        //alert(CollisionBox[0]);

                        //alert(xnext);
                        //alert(ynext);
                        //alert(headings);





                    }

            }

        }




}

    function Uniquer(value, index, self) { 
        return self.indexOf(value) === index;
    }

    function stripUndefined (arr) {
      return arr.reduce(function (result, item) {
        result.push( Array.isArray(item) && !item.length ? stripUndefined(item) : item );
        return result;
      }, []);
    }

    </script>

</html>
