<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">

        <title>Firefly</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="field">

            <div id="left" class="full-height">

                <div class="leftboxtop">

                        <h2 class="mainlabel">Shop Statistics</h2>
                        <div class="line2">
                        </div>

                </div>

                <div class="leftbox">

                    <p>id: {!!Form::label('id', $id)!!}</p>
                    <p>width: {!!Form::label('x', $x)!!}</p>
                    <p>height: {!!Form::label('y', $y)!!}</p>

                    <div class="line2">
                    </div>

                </div>

                <div class="leftbox">

                    <p>Create an new Robot:</p>

                    {!! Form::open(array('method' => 'post', 'action' => 'Controller@createRobot')) !!}
                    {!! Form::hidden('id', $id) !!}
                    {!! Form::number('x', null, array ('placeholder' => 'X', 'maxlength' => 2, 'class' => 'form-control')) !!}
                    <br>
                    {!! Form::number('y', null, array ('placeholder' => 'Y', 'maxlength' => 2, 'class' => 'form-control')) !!}
                    <br>
                    {!! Form::text('heading', null, array ('placeholder' => 'Heading', 'maxlength' => 1, 'class' => 'form-control')) !!}
                    <br>
                    {!! Form::text('commands', null, array ('placeholder' => 'Commands', 'maxlength' => 30, 'class' => 'form-control')) !!}
                    <br>
                    {!! Form::submit('Create Robot', ['class' => 'btn btn-success']) !!}

                    {!! Form::close() !!}

                    <br>

                    <div class="cells"><a id="back" href="control" class="btn btn-warning">Back</a></div>

                </div>

            </div>

            <div class="contents">


                <div id="top">

                    <div class="title m-b-md">
                        Firefly
                        <div id="line">
                        </div>
                    </div>

                </div>

                <div id="initial2">

                    <div id="editRobotInner">  

                            <div id="LogsTitle">
                                {!!Form::label('message', $message, ['class' => 'lighttext fontBoost2'])!!}
                            </div>

                            @if ($owner != null)

                            <div id="editLogBox">

                            @foreach ($owner as $data)
                            <div style="width:100%!important;height:50px;padding-left:20px;">
                                <!--<div class="cells"><a href="editShop/{{$id}}/{{$data -> robot_id}}" class="button">Delete</a></div>-->
                                <div class="cell-small"><a onclick='Initialize({{$data->robot_id}});' class="btn btn-info">Edit this Robot</a></div>
                                <div class="cells">{!! Form::open(array('method' => 'delete', 'action' => ['Controller@deleteRobot'])) !!}
                                {!! Form::hidden('id', $id) !!}
                                {!! Form::hidden('robotid', $data->robot_id) !!}
                                <div class="cell-smaller">{!! Form::submit('Delete this Robot', ['class' => 'btn btn-warning']) !!}</div>
                                {!! Form::close() !!}
                                </div>
                                <div class="cell-smaller"><p class="darktext">id: {!!Form::label($data -> robot_id)!!}</p></div>
                                <div class="cell-smaller"><p class="darktext">x-location: {!!Form::label($data -> x)!!}</p></div>
                                <div class="cell-smaller"><p class="darktext">y-location: {!!Form::label($data -> y)!!}</p></div>
                                <div class="cell-smaller"><p class="darktext">heading: {!!Form::label($data -> heading)!!}</p></div>
                                <div class="cell-smaller"><p class="darktext">commands: {!!Form::label($data -> commands)!!}</p></div>
                            </div>
                            <br>
                            @endforeach
                            </div>
                            @endif

                    </div>


                </div>

            </div>
        </div>

        <div id="overlayOuter1" onclick="Initialize(-1)">
        </div>

        <div id="overlayInnerx">
            @if ($owner != null)
                    {!! Form::open(array('method' => 'put', 'action' => 'Controller@editRobot', 'class' => 'Divider')) !!}
                    {!! Form::hidden('id', $id) !!}
                    {!! Form::hidden('robot_id', ' ', array ('placeholder' => '', 'id' => 'replacer')) !!}
                    <h2 class="Division ShiftUp fontBoost2">Editing Robot ID:</h2>
                    {!! Form::label('robot_id', $data -> robot_id, array ('placeholder' => '', 'id' => 'replacer2', 'class' => 'Division fontBoost2')) !!}
                    {!! Form::number('x', null, array ('placeholder' => '', 'maxlength' => 2, 'class' => 'form-control Division SizeBoost fontBoost1')) !!}
                    {!! Form::number('y', null, array ('placeholder' => '', 'maxlength' => 2, 'class' => 'form-control Division SizeBoost fontBoost1')) !!}
                    {!! Form::text('heading', null, array ('placeholder' => '', 'maxlength' => 1, 'class' => 'form-control Division SizeBoost fontBoost1')) !!}
                    {!! Form::text('commands', null, array ('placeholder' => '', 'maxlength' => 30, 'class' => 'form-control Division SizeBoost fontBoost1')) !!}
                    {!! Form::submit('Confirm Edit', ['class' => 'button', 'class' => 'btn btn-info Division SizeBoost fontBoost1']) !!}</div>
                    {!! Form::close() !!}
            @endif
        </div>

        </div>
    </body>

    <script async>

    var Visible = false;
    var reference = document.getElementById('replacer');
    var reference2 = document.getElementById('replacer2');
    $('body').css("overflow-y", "hidden");

    function Initialize(id) {


        //alert(id + " " locx + " " locy + " " heading + " " commands);

        if (Visible == false) {

        reference.value = id;
        reference2.innerHTML = id;

        document.getElementById('overlayOuter1').style.display = "block";
        document.getElementById('overlayInnerx').style.display = "flex";

        Visible = true;
        setTimeout(Open, 100);

        }

        else {

        document.getElementById('overlayOuter1').style.opacity = "0";
        document.getElementById('overlayInnerx').style.opacity = "0";
        Visible = false;
        setTimeout(Close, 500);

        }
    }

    function Open() {

        document.getElementById('overlayOuter1').style.opacity = "0.6";
        document.getElementById('overlayInnerx').style.opacity = "1";

    }

    function Close() {

        document.getElementById('overlayOuter1').style.display = "none";
        document.getElementById('overlayInnerx').style.display = "none";

    }

    </script>


</html>
