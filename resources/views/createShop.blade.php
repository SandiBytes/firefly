<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ URL::asset('js/jquery-3.2.0.min.js') }}"></script>

        <title>Firefly</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">

                <div id="main-menu-top">

                    <div class="title m-b-md">
                        Firefly
                    </div>

                </div>

                <div id="main-menu-bottom">

                    <div class="links">

                        {!! Form::open(array('action' => 'Controller@createShop')) !!}
                        <div class="arranger ShiftDown"><a href="control" class="btn btn-info fontBoost1">Back</a></div>
                        <div class="arranger">{!! Form::number('x', null, array ('placeholder' => '', 'maxlength' => 2, 'class' => 'form-control')) !!}</div>
                        <div class="arranger">{!! Form::number('y', null, array ('placeholder' => '', 'maxlength' => 2, 'class' => 'form-control')) !!}</div>
                        <div class="arranger ShiftDown">{!! Form::submit('Create New Shop', ['class' => 'btn btn-info fontBoost1']) !!}</div>
                        {!! Form::close() !!}

                        {!!Form::label('data', $alert, ['class' => 'lighttext'])!!}

                    </div>

                </div>

                    <div id="fancybox">
                    <div id="fancyline">
                    </div>

                </div>

            </div>
        </div>
        <div id="overlayOuter" class="flex-center overlay">

                <div id="overlayInner" class="modaline">

                </div>

        </div>
    </body>

    <script async>

    $(document).ready( Execute );

    function Execute() {
 
        setTimeout(Transform, 500);

        $("#main-menu-top").css("background-color", "#0080ff");
        $("#main-menu-bottom").css("background-color", "#ac00e6");

    }

    function Transform() {

        $("#main-menu-top").css("padding-top", "25vh");
        $("#fancyline").css("width", "100%");

    }


    </script>


</html>
