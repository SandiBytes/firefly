<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRobotunitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('robotunits', function (Blueprint $table) {
            $table->increments('id');
            $table->int('owner')->nullable();
            $table->int('x')->nullable();
            $table->int('y')->nullable();
            $table->string('heading')->nullable();
            $table->string('commands')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
